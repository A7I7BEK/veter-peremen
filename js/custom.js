


/* Mobile Menu
============================================================*/
$(document).on('click', '.nav_menu_btn, .nav_menu_bg', function () {
	$('.nav_menu_btn').toggleClass('active');
	$('.nav_menu').toggleClass('active');
	$('.nav_menu_bg').toggleClass('active');
	$('html, body').toggleClass('ov-h');
});
/*========== Mobile Menu ==========*/





/* Banner
============================================================*/
$(document).ready(function () {
	try {
		var owl = $('.banner');

		owl.owlCarousel({
			// autoPlay: 5000,
			stopOnHover: true,
			singleItem: true,
			autoHeight : true,
			navigation: false,
			// pagination: false,
			slideSpeed: 300,
			paginationSpeed: 400,
			transitionStyle : 'fadeUp'
		});

		// Custom Navigation Events
		$('.banner_btn.prev').click(function () {
			owl.trigger('owl.prev');
		});

		$('.banner_btn.next').click(function () {
			owl.trigger('owl.next');
		});

	}
	catch (e) {
		console.warn("Owl Carousel cannot find .banner");
	}
});
/*========== Banner ==========*/





/* Comment Banner
============================================================*/
$(document).ready(function () {
	try {
		var owl = $('.comment_bnr');

		owl.owlCarousel({
			// autoPlay: 4000,
			stopOnHover: true,
			itemsCustom : [
				[0, 1],
				// [480, 2],
				[768, 2],
				[1200, 3],
			],
			// autoHeight : true,
			navigation: false,
			pagination: false,
			slideSpeed: 300,
			paginationSpeed: 400,
		});

		// Custom Navigation Events
		$('.comment_bnr_btn.prev').click(function () {
			owl.trigger('owl.prev');
		});

		$('.comment_bnr_btn.next').click(function () {
			owl.trigger('owl.next');
		});

	}
	catch (e) {
		console.warn("Owl Carousel cannot find .comment_bnr");
	}
});
/*========== Comment Banner ==========*/





/* Partner
============================================================*/
$(document).ready(function () {
	try {
		var owl = $('.partner_bnr');

		owl.owlCarousel({
			// autoPlay: 4000,
			stopOnHover: true,
			itemsCustom : [
				[0, 1],
				[480, 3],
				[992, 4],
				[1200, 5],
			],
			// autoHeight : true,
			navigation: false,
			pagination: false,
			slideSpeed: 300,
			paginationSpeed: 400,
		});

		// Custom Navigation Events
		$('.partner_bnr_btn.prev').click(function () {
			owl.trigger('owl.prev');
		});

		$('.partner_bnr_btn.next').click(function () {
			owl.trigger('owl.next');
		});

	}
	catch (e) {
		console.warn("Owl Carousel cannot find .comment_bnr");
	}
});
/*========== Partner ==========*/






/* Banner Mouse Scroll
============================================================*/
$(document).on('click', '.banner_mouse', function (e) {
	e.preventDefault();

	var offset_top = $('.advantage_sec').offset().top + 10;
	$('html, body').animate({ scrollTop: offset_top }, 700);

});
/*========== Banner Mouse Scroll ==========*/







/* Anchor
============================================================*/
$(window).scroll(function () {
	if ($(window).scrollTop() > 800)
	{
		$('.anchor').addClass('active');
	}
	else
	{
		$('.anchor').removeClass('active');
	}
});

$(document).on('click', '.anchor', function () {

	$('html, body').animate({ scrollTop: 0 }, 700);

});
/*========== Anchor ==========*/








/* Modal Form
============================================================*/
$(document).on('click', '[data-send-form]', function (e) {
	e.preventDefault();

	$(this).closest('.base_modal_cnt').removeClass('active').siblings('.base_modal_cnt.thank').addClass('active');
});
/*========== Modal Form ==========*/








/* Tourism Place
============================================================*/
$(document).ready(function () {
	for (i = 0; i < 4; i++) {

		try {
			$('#place-banner-' + i).slick({
				slidesToShow: 1,
				swipeToSlide: true,
				slidesToScroll: 1,
				autoplay: true,
				autoplaySpeed: 2000,
				arrows: false,
				fade: true,
				asNavFor: '#place-banner-thumb-' + i
			});
		}
		catch (e) {
			console.warn('Slick cannot find #place-banner-' + i);
		}


		try {
			$('#place-banner-thumb-' + i).slick({
				slidesToShow: 4,
				swipeToSlide: true,
				slidesToScroll: 1,
				prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
				nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>',
				focusOnSelect: true,
				asNavFor: '#place-banner-' + i
			});
		}
		catch (e) {
			console.warn('Slick cannot find #place-banner-thumb-' + i);
		}

	}
});
/*========== Tourism Place ==========*/









/* Tourism Direction More
============================================================*/
$(document).on('click', '.tour_di_show_btn', function (e) {
	e.preventDefault();

	var tourPlace = $('.tour_place_sec');
	tourPlace.addClass('active');

	var offset_top = tourPlace.offset().top;
	$('html, body').animate({ scrollTop: offset_top }, 700);
});
/*========== Tourism Direction More ==========*/





/* Tourism Place More
============================================================*/
$(document).on('click', '.tour_place_it_mr_btn', function (e) {
	e.preventDefault();

	$('.tour_place_it_mr_btn').removeClass('active');
	$('.tour_place_it_desc').slideUp();



	$(this).addClass('active');
	$(this).closest('.tour_place_it').find('.tour_place_it_desc').slideDown(function () {

		$('html, body').animate({ scrollTop: $(this).offset().top }, 700);

		for (i = 0; i < 4; i++) {
			$('#place-banner-' + i).slick('setPosition');
			$('#place-banner-thumb-' + i).slick('setPosition');
		}
	});

});
/*========== Tourism Place More ==========*/



















/* Header
============================================================*/

/*========== Header ==========*/


